---
title: FAQ
---

## What can I contribute?
Generally, there is a wide range of contributions that can be made to the firmware, the IOs and Android apps, the various methods of BLE communication, the user interface, the documentation, by reviewing existing work, and of course by creating your own [interhacktions](/en/interhacktions)!

If you don't know where to get started you can also have a look at some of the ideas for contributions we outlined [here](https://git.card10.badge.events.ccc.de/card10/logix/issues). If you have questions on the issue, please don't hesitate to comment or get in touch to the creator of the issue through our [Matrix](https://en.wikipedia.org/wiki/Matrix_(protocol)):( [`#card10badge:asra.gr`](https://matrix.to/#/#card10badge:asra.gr))/[IRC](https://en.wikipedia.org/wiki/Internet_Relay_Chat) ( [`freenode.com#card10badge`](ircs://chat.freenode.net:6697/card10badge)) channel.

## How does the so-called wiki work?
To keep things easier to maintain (the r0ket wiki has been going for 8 years now!), we decided on a git/markdown based wiki for this year. If you have any questions, don't hesitate to get in touch via the card10 communication channels.

## What components are on card10?
Have a look at the [hardware overview](/en/hardware-overview).

## Is there a recommended dev board?
Since the BLE chip is very new, none of the card10 researches and travelers have found a suitable dev boards to recommend. We will try to share the research prototypes as widely as possible through hackerspaces. If you want to get your hands on a card10 prototype, first see if a hackerspace near you already has a [card10logy](/en/prototype_distribution) department. If this is not the case, please get in touch via Matrix Chat or IRC to suggest opening a new card10logy department.

## How can I get a prototype?
You can find out if there's an ambassador in your city or a city near you. If there isn't maybe you want to become an ambassador yourself and help with the card10logix community.
More detailed info you can find [here](/en/prototype_distribution)

## What should I bring to CCCamp 2019 so I can enjoy card10?
*I have noticed that the card10 needs some maintenance; most people carrying a card10 have brought their own tools for maintaining their card10, making it more individual, or adding functionality. In some places, maintenance stations are available, where card10's can be recharged, and tools and knowledge is shared*

According to current research, it is recommended to carry the following:

* A USB-C cable for charging and programming
* A screwdriver for assembling card10 (more details following soon)
* A needle for sewing to the card10 wristband
* A device capable of communicating via [Bluetooth Low Energy](/en/ble) (BLE)

## Will the wristband be included with the card10?
*Whilst at most times #card10 is observed wrist mounted on a very soft, fluffy wrist band, occasionally I spotted a card10 in unusual places, fitted on top of clothing, extended with more LEDs and sometimes also combined with other electronic boards. These boards often have the shape of r0kets, but I also noticed several other creative outlines and colourful blinking patterns.*

Yes, a comfortable neoprene wristband comes with every card10. If you want something else, you can of course bring your own, and attach it either with the screw bars that also attach the neoprene wristband, or by sewing your card10 onto your wristband. The latter is also an option with the neoprene wrist band, if you are concerned about an allergic reaction to the metal clamps, however this reduces the EKG functionality. To maintain skin contact and thus EKG function, you can instead attach a piece of conductive silver fabric on the skin side of your wristband and connect it to the EKG (TODO: add more details how to do this). You can even attach your card10 to anything else you can come up with, using the screwbars or needle and thread!

We recommend reinforcing the velcro hook side attachment by adding a seam along the sides of the velcro hook side piece.
