---
title: Events
---

- Upcoming:
 - **card10 Soldering Session in Berlin**: xHain Wed-Thurs 14&15. August starting at 10 am
- Previous
 - card10 Soldering Session in Berlin: xHain Sat-Sun 10.-11. August starting at 10 am
 - card10 meetup in Berlin: xHain 06.08. ~~19:00~~ => 20:00
 - card10 meetup in Berlin: xHain 05.06. 20:00
 - card10 meetup in Berlin: afra 21.06. 19:00
 - card10 meetup in Berlin: xHain 14.06. 19:00
 - card10 at the [GPN19](https://entropia.de/GPN19:There_will_have_been_a_camp_badge:_We%27re_reconstructing_future_technology_and_you_can_help_us_with_this_mission)
 - CCCamp2019 Badge Workshop at #eh19: [Description](https://conference.c3w.at/eh19/talk/VHFMJ7/), 2019-04-21, 14:30–15:30
 - CCCamp 2019 Badge at #eh19 (easterhegg2019): [Description](https://conference.c3w.at/eh19/talk/DA7KTT/), [Recording](https://media.ccc.de/search/?q=CCCamp+Badge+Talk)
